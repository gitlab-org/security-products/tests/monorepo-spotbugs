package security.sample.helper
import java.io.File
import java.io.IOException
import java.net.URISyntaxException

class FileHelper {

	@Throws(IOException::class, URISyntaxException::class)
	fun createDirectory(){
		val filepath = "file.md";
		//unsafe - File Traversal
		createTempDir(filepath, filepath)
		createTempDir(filepath, filepath, File("static"))

		// Safe
		createTempDir()
	}
}